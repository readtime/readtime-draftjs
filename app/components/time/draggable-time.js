import React from 'react';
import Draggable from 'react-draggable';

import Time from './time.js';

require('./draggable-time-indicator.less');

export default props => {
  let draggableProps = {
    bounds: 'parent'
  };
  return (
    <Draggable {...draggableProps}>
      <Time
        parentElementName={props.parentElementName}
        time={props.time}/>
    </Draggable>
  );
};
