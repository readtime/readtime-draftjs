import React from 'react';
import cx from 'classnames';
import {selectTimeUnitsToShow, zeroPad} from '../../utils/common.js';

require('./time.less');

const Time = (props) => {
  const timeToShow = selectTimeUnitsToShow(props.time);
  const timeSpans = [];
  const keys = Object.keys(timeToShow);
  const dividers = {
    'days': ' ',
    'hours': ':',
    'minutes': ':',
    'seconds': '.',
    'milliseconds': '',
  };
  const possibleKeys = [
    'days',
    'hours',
    'minutes',
    'seconds',
    'milliseconds',
  ];
  possibleKeys
    .filter(key => keys.indexOf(key) !== -1)
    .slice(0, 3)
    .forEach((timeUnit, index) => {
      const divider = (index < 2) ? dividers[timeUnit] : '';
      timeSpans.push(
        <span
          className={`time--unit time--unit__${timeUnit}`}
          key={timeUnit}
          title={`${timeToShow[timeUnit]} ${timeUnit}`}>
            {zeroPad(timeToShow[timeUnit]) + divider}
        </span>
      );
    });
  const biggestKey = possibleKeys.find(key => {
    return keys.indexOf(key) !== -1;
  });
  const subLabel = !props.showUnit ? null : (
                                     <span className='time--sublabel'>{biggestKey}</span>
                                   );
  const classes = cx(
    {
      [`${props.parentElementName}--time`]: props.parentElementName,
      [`time__biggest-sublabel-is-${biggestKey}`]: biggestKey
    },
    'time'
  );

  return (
    <div className={classes}>
      {timeSpans}
      {subLabel}
    </div>
  );
};

Time.defaultProps = {
  showUnit: true,
  zeroPad: false
};

export default Time;