import React, {Component} from 'react';
import cx from 'classnames';
import Draft from 'draft-js';
import ReactGA from 'react-ga';

import TimeIndicator from '../controls/time-indicator.js';
import Slider from '../slider/slider.js';
import Footer from '../footer/footer.js';
import SidebarButton from './sidebar-button.js';
import Stopwatch from '../stopwatch/stopwatch.js';

import commands from '../../constants/commands.js';
import people from '../../constants/people.js';

import calculate from '../../utils/calculate.js';
import extractRawTextFromEditorState from '../../utils/extract-raw-text-from-editorstate.js';
import getTotalBreakTimes from '../../utils/get-total-break-times.js';

const {convertToRaw} = Draft;

require('./sidebar.less');

export default class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.handleClickAddDescription = this.handleClickAddDescription.bind(this);
    this.handleClickAddBreak = this.handleClickAddBreak.bind(this);
    this.handleClickCloseStopwatch = this.handleClickCloseStopwatch.bind(this);
    this.handleClickSaveSpeed = this.handleClickSaveSpeed.bind(this);
    this.handleClickShare = this.handleClickShare.bind(this);
    this.handleClickStopWatch = this.handleClickStopWatch.bind(this);
    this.handleMouseDownSidebarButtonWrapper = this.handleMouseDownSidebarButtonWrapper.bind(this);
    this.handleChangeSlider = this.handleChangeSlider.bind(this);
    this.handleClickToggleSidebar = this.handleClickToggleSidebar.bind(this);
  }

  handleChangeSlider(newValue) {
    if (newValue === this.props.sliderValue) {
      return null;
    }
    this.props.onChangeSlider(newValue);
  }

  handleClickToggleSidebar() {
    this.props.toggle()
  }

  handleClickAddBreak() {
    this.props.onClickAddBreak(event);
  }

  handleClickAddDescription() {
    this.props.onClickAddDescription(event);
  }

  handleClickShare() {
    this.props.onClickShare();
  }

  handleClickStopWatch() {
    this.openStopWatch();
  }

  setSidebarView(targetView = 'default') {
    this.props.setView(targetView);
  }

  handleClickCloseStopwatch() {
    this.openDefaultView();
  }

  handleClickSaveSpeed(elapsedTime) {
    const newSliderValue = this.extractClosestPersonMatch(elapsedTime);

    this.props.onClickUseYourSpeed(newSliderValue);
    this.openDefaultView();
    this.props.close()
  }

  handleMouseDownSidebarButtonWrapper(e) {
    e.preventDefault();
  }

  setSidebarView(targetView = 'default') {
    this.props.setView(targetView);
  }

  openStopWatch() {
    this.props.setView('stopwatch');

    ReactGA.event({
      category: 'sidebar',
      action: 'toggle view',
      label: 'stopwatch',
    });
  }

  openDefaultView() {
    this.props.setView('default')
    ReactGA.event({
      category: 'sidebar',
      action: 'toggle view',
      label: 'default',
    });

  }

  extractClosestPersonMatch(elapsedTime) {
    const rawContent = convertToRaw(this.props.editorState.getCurrentContent());
    const text = extractRawTextFromEditorState(rawContent);
    const breakTimes = getTotalBreakTimes(rawContent);

    const times = [];
    const timesPerPerson = {};
    const keys = Object.keys(people);
    keys.map(personKey => {
      const {totalSeconds} = calculate({
        text,
        breakTimes,
        multiplier: personKey,
      });
      const diff = Math.abs(elapsedTime - totalSeconds);
      timesPerPerson[diff] = personKey;
      times.push(diff);
    });

    const match = times.reduce(function (prev, curr) {
      return (Math.abs(curr - elapsedTime) < Math.abs(prev - elapsedTime) ? curr : prev);
    });
    return timesPerPerson[match];
  }

  renderButtons() {
    const {isInitial, focusedButton} = this.props;
    const handlers = {
      break: this.handleClickAddBreak,
      description: this.handleClickAddDescription,
      stopwatch: this.handleClickStopWatch,
      share: this.handleClickShare,
    };
    const disabled = {
      break: isInitial,
      description: isInitial,
      stopwatch: false,
      share: isInitial,
    };

    commands.map(command => command.onClick = handlers[command.command]);
    commands.map(command => command.disabled = disabled[command.command]);

    return (
      <div
        className='sidebar--buttons' key='buttons-container'
        onMouseDown={this.handleMouseDownSidebarButtonWrapper}>
        {commands.map(button => (
          <div
            key={button.command}
            onMouseDown={e => {
              e.preventDefault()
            }}
          >
            <SidebarButton
              isActive={button.command === (focusedButton || "").toLowerCase()}
              {...button}
            />
          </div>
        ))}
      </div>
    );
  }

  renderStopWatch() {
    return (
      <Stopwatch
        time={this.props.time}
        sliderValue={this.props.sliderValue}
        onClickClose={this.handleClickCloseStopwatch}
        onClickSaveSpeed={this.handleClickSaveSpeed}
      />
    );
  }

  renderDefaultContent() {
    return [
      <TimeIndicator
        key='editor-controls'
        time={this.props.time}
      />,
      <Slider
        showPersonBookmark={() => {
          this.props.showPersonBookmark();
          this.props.close();
        }}
        key='slider'
        value={this.props.sliderValue}
        onChange={this.handleChangeSlider}
      />,
      this.renderButtons(),
      <Footer
        key='footer'
      />,
    ];
  }

  render() {
    const classes = cx(
      'sidebar',
      {
        'sidebar__opened': this.props.open,
      },
    );

    const content = {
      default: this.renderDefaultContent.bind(this),
      stopwatch: this.renderStopWatch.bind(this),
    }[this.props.activeView]();

    return (
      <div className={classes}>
        <div className="sidebar--contents">
          {content}
        </div>
      </div>
    )
  }
}
