import React from 'react';
import cx from 'classnames';

import Icon from '../common/icon/icon.js';

require('./sidebar-button.less');

const getOs = (platform) => {
  if (platform.indexOf('Mac') > -1) { return 'osx'; }
  if (platform.indexOf('Win') > -1) { return 'windows'; }
  if (platform.indexOf('Linux') > -1) { return 'linux'; }
  return 'unknown';
};
const osButtonName = {
    'osx': 'cmd',
    'windows': 'ctrl',
    'linux': 'ctrl',
  }[getOs(navigator.platform)] || 'ctrl';

const SidebarButton = props => {
  let classes = cx(
    'button',
    'sidebar-button',
    [`button--${props.command}`],
    {
      'sidebar-button__active': props.isActive
    }
  );
  const buttonProps = {
    key: `button-${props.command}`,
    onClick: props.onClick,
    className: classes,
    disabled: props.disabled
  };

  if (props.button) {
    buttonProps.title = `${props.label}. Shortcut ${osButtonName} + ${props.button}`;
  }

  return (
    <button {...buttonProps}>
      <Icon
        iconId={props.icon || props.command}
        parentElementName="sidebar-button"/>
      <span className='sidebar-button--label'>{props.label}</span>
    </button>
  );
};

SidebarButton.defaultProps = {
  disabled: false
};

export default SidebarButton;