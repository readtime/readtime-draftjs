import React from 'react';
import cx from 'classnames';
import transitionEndEventName from '../../utils/find-transition-end-event-name.js';
require('./bookmark.less');

const transitionEndEvent = transitionEndEventName();

export default class Bookmark extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      visible: false
    };

    this.defaultProps = {
      showCut: true
    };

    this.componentWillUnmount = () => {
      this.props.onClickClose();
      this.el.removeEventListener(transitionEndEvent, this.props.onClickClose);
    };

    this.componentDidMount = () => {
      setTimeout(() => {
        this.setState({
          visible: true
        });
      }, 16);
    };

    this.handleClickClose = () => {
      this.setState({
        visible: false
      });
      this.el.addEventListener(transitionEndEvent, this.props.onClickClose);
    }
  }

  render () {
    const {
      showCut,
      children
    } = this.props;
    const {visible} = this.state;
    const classes = cx('bookmark', {
      'bookmark__invisible': !visible,
      'bookmark__nocut': !showCut
    });

    return (
      <div
        className={classes}
        ref={(ref) => {this.el = ref;}}
      >
        <button
          className='bookmark--close-button'
          onClick={this.handleClickClose}>
          X
        </button>
        {children}
      </div>
    );
  }
};

