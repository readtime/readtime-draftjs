import React from 'react';

require('./person.less');

export default ({img, bio, name, variant}) => {
  const filePath = img ? require(`../../images/speakers/${img}${variant}.png`) : null;
  return (
    <div className='person'>
      {img &&
        <img
          className='person--photo'
          src={filePath}
          alt={`Photo of ${name}`}
        />
      }
      <div className='person--info'>
        <h1 className='person--name'>{name}</h1>
        <p className='person--bio'>{bio}</p>
      </div>
    </div>
  );
};