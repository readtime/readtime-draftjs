import React, {Component} from 'react';
import cx from 'classnames';

import commands from '../../constants/commands.js';
import Time from '../time/time.js';
import Icon from '../common/icon/icon.js';

require('./mobile-menu.less');

const Buttons = ({isInitial, focusedButton, handlers}) => {
  const disabled = {
    break: isInitial,
    description: isInitial,
    stopwatch: false,
    share: isInitial,
  };

  commands.map(command => command.onClick = handlers[command.command]);
  commands.map(command => command.disabled = disabled[command.command]);

  return (
    <div className='mobile-menu--buttons'>
      <button
        className='mobile-menu--button mobile-menu--button__hamburger'
        onClick={handlers.toggle}
        key={'hamburger'}
        title={'Toggle Menu'}
      >
        <Icon
          iconId='hamburger'
          parentElementName="mobile-menu"/>
      </button>
      {commands.map(button => (
        <span
          key={button.command}
          onMouseDown={e => {
            e.preventDefault()
          }}
        >
          <button
            className={cx(
              'mobile-menu--button',
              `mobile-menu--button__${button.command}`,
              {
                'mobile-menu--button__active': button.command === (focusedButton || "").toLowerCase(),
              },
            )}
            disabled={button.disabled}
            onClick={button.onClick}
            title={button.label}
          >
            <Icon
              iconId={button.icon || button.command}
              parentElementName="mobile-menu"/>
          </button>
        </span>
      ))}
    </div>
  );

}

export default class MobileMenu extends Component {
  render() {
    const {time, isInitial, focusedButton} = this.props;
    const handlers = {
      break: this.props.onClickAddBreak,
      description: this.props.onClickAddDescription,
      share: this.props.onClickShare,
      toggle: this.props.onClickToggleSidebar,
    };

    return (
      <div className='mobile-menu'>
        <Buttons
          handlers={handlers}
          isInitial={isInitial}
          focusedButton={focusedButton}
        />
        <Time
          parentElementName='mobile-menu'
          time={time}
        />
      </div>
    )
  }
}
