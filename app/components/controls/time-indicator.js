import React from 'react';
import Icon from '../common/icon/icon.js';
import Time from '../time/time.js';

require('./editor-controls.less');

export default props => {
  return (
    <div className='editor-controls'>
      <Icon
        className='time--logotype'
        iconId='logotype'/>
      <Time
        parentElementName='editor-controls'
        time={props.time}/>
    </div>
  );
};
