import React from 'react';
import cx from 'classnames';
import ReactGA from 'react-ga';
import CopyToClipboard from 'react-copy-to-clipboard';

import Icon from '../common/icon/icon.js';
import { breakDescriptionPattern } from '../../constants/patterns.js';

require('./share.less');

const prepareEmailtext = ({savedItemId, text}) => {
  let urlParam = '';
  if (savedItemId) {
    urlParam = `/x/${savedItemId}`;
  }
  return `Hey [YOUR FRIENDS NAME], 
  
I wanted to share this text i\'ve been working over at readtime.eu with you.
You can see and edit the full text on http://readti.me${urlParam} for free.

This is a part of the text i\'ve written:

--------------------
${text.blocks.slice(0, Math.ceil(text.blocks.length/2))
    .map(part => {
      if (part.type !== 'atomic') {
        return part.text;
      }
      return `
      -----
      This takes ${part.text
        .split(breakDescriptionPattern)
        .join(` seconds
      `)}
      -----
`;
    })
    .join('\n\n')}
--------------------

Go to http://readti.me${urlParam} to read the rest.

Cheers,
[YOUR NAME]`;
};

const renderButtons = (props) => {
  const emailText = prepareEmailtext(props);
  const buttons = [
    {
      icon: 'facebook',
      label: 'Share on Facebook',
      onClick: props.handleClickFacebook
    },
    {
      icon: 'twitter',
      label: 'Share on Twitter',
      onClick: props.handleClickTwitter
    },
    {
      icon: 'email',
      label: 'Share via Email',
      onClick: props.handleClickEmail,
      href: `mailto:?subject=Readtime&body=${escape(emailText)}`
    }
  ];
  return (
    <div className='share-buttons'>
      {buttons.map(button => renderButton(button))}
    </div>
  )
};

const renderButton = ({href, icon, label, onClick}) => {
  const classes = cx(
    'share-button',
    {
      [`share-button__${icon}`]: icon
    }
  );

  return React.createElement(
    href ? 'a' : 'button',
    {
      className: classes,
      key: icon,
      onClick,
      href
    }, (
      <Icon
        iconId={icon}
        title={label}
        parentElementName='share'
      />
    )
  );
};

const handleClickCopy = () => {
  ReactGA.event({
    category: 'social',
    action: 'share',
    label: 'copy'
  });
}

export default props => {
  const href = props.savedItemId ? `readti.me/x/${props.savedItemId}` : 'Saving...';

  return (
    <div className='share'>
      <div className='share--background share--background-image'></div>
      <div className='share--background share--background-color'></div>
      <div className='share--content'>
        <h1 className='share--header'>Share you work with friends</h1>
        <p className='share--info'>When working you might find the need to share you result with your teammates. We also
          store the speed setting you've set.</p>
      </div>
      <CopyToClipboard
        text={`http://${href}`}>
        <button className='share--link' title='Copy to clipboard' onClick={handleClickCopy}>{href}</button>
      </CopyToClipboard>
      <div className='share--content'>
        {renderButtons(props)}
      </div>
    </div>
  );
};
