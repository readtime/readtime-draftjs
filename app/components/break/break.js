import React from 'react';
import cx from 'classnames';
import {Entity} from 'draft-js';
import PATTERNS from '../../constants/patterns.js';


import { default as TextArea } from 'react-textarea-autosize';

require('./break.less');

const MIN_WIDTH = 23;
const WIDTH_PER_CHARACTER = 12;

const MAX_VALUE = 99999;

export default class Break extends React.Component {
  constructor (props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleBlurDescription = this.handleBlurDescription.bind(this);
  }


  shouldComponentUpdate (nextProps) {
    const { blockProps } = this.props;
    return (blockProps.time !== nextProps.blockProps.time)
      || (blockProps.description !== nextProps.blockProps.description)
      || (blockProps.disabled !== nextProps.blockProps.disabled)
  }

  handleChange () {
    const value = [
      this.input.value,
      this.props.blockProps.description
    ].join(PATTERNS.breakDescriptionPattern);
    this.props.blockProps.onChange(this.props.block, value, this.props.blockProps.getEditorState);
  }

  handleBlurDescription (event) {
    const value = [
      this.input.value,
      event.target.value
    ]
      .join(PATTERNS.breakDescriptionPattern);
    this.props.blockProps.onChange(this.props.block, value, this.props.blockProps.getEditorState);
  };

  render () {
    const {
      block,
      blockProps,
      offsetKey
    } = this.props;
    const {
      time,
      description,
      disabled,
    } = blockProps;
    const inputStyle = {
      width: (time.length * WIDTH_PER_CHARACTER) + MIN_WIDTH
    };
    const classes = cx(
      'break-indicator',
      {
        'break-indicator__with-description': description && description.length
      }
    );
    const contentBlockKey = block.getKey();
    const labelPre = `this takes`;
    const labelPost = `second${[
      's',
      '',
      's'
    ][Math.min(time, 2)]}`;
    const descriptionDiv = (
      <TextArea
        placeholder="What's happening here? Why do we have a break?"
        className="break-description"
        defaultValue={description}
        onFocus={(event) => {
          blockProps.onFocus(event);
        }}
        onBlur={(event) => {
          blockProps.onBlur(event);
          this.handleBlurDescription(event)
        }}
        disabled={disabled}
        title={'Lets have a break'}
      />
    );
    const inputProps = {
      style: inputStyle,
      id: `input-${contentBlockKey}`,
      className: 'break-indicator--input',
      type: 'number',
      value: Number(time) || 1,
      min: 1,
      max: MAX_VALUE,
      ref: (element) => {
        this.input = element;
      },
      onFocus: blockProps.onFocus,
      onBlur: blockProps.onBlur,
      onChange: this.handleChange,
      disabled,
    };

    return (
      <div
        data-offset-key={offsetKey}
        className={classes}>
        {descriptionDiv}
        <div className="break-wrap">
          <div className="break-indicator--bar"/>
          <label
            htmlFor={`input-${contentBlockKey}`}
            className="break-indicator--label">
            <span className="break-indicator-label break-indicator-label--pre">{labelPre}</span>
            <input {...inputProps}/>
            <span className="break-indicator-label break-indicator-label--post">{labelPost}</span>
          </label>
        </div>
      </div>
    );
  }
};
