import React from 'react';
import cx from 'classnames';

require('./icon.less');

export default props => {
  const {
    iconId,
    className,
    parentElementName,
    title
  } = props;
  if (!iconId) {
    return null;
  }
  const classes = cx(
    className,
    {
      [`${parentElementName}--icon__${iconId}`]: iconId && parentElementName,
      [`${parentElementName}--icon`]: iconId && parentElementName,
      [`icon__${iconId}`]: iconId
    },
    'icon'
  );

  return (
    <svg className={classes} title={title}>
      <use xlinkHref={`#${iconId}`}/>
    </svg>
  )
};