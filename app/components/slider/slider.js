import React, { Component } from 'react';
import cx from 'classnames';

require('./slider.less');

import people from '../../constants/people.js';

const DOT_WIDTH = 20;
const SIDE_MARGIN = 20;
const DOT_OFFSET = 4;

class Slider extends Component {
  constructor(props) {
    super(props)
    this.handleChangeValue = this.handleChangeValue.bind(this);
    this.handleWindowResize = this.handleWindowResize.bind(this);

    this.state = {
      sidebarWidth: 332
    }
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowResize);
  }

  componentDidMount() {
    this.updateSidebarWidth()
  }

  handleChangeValue(event) {
    this.props.onChange(event.target.value);
  }

  handleWindowResize () {
    this.updateSidebarWidth()
  }

  updateSidebarWidth() {
    const currentSidebarWidth = document.querySelector('.sidebar').offsetWidth;
    if (currentSidebarWidth !== this.state.sidebarWidth) {
      this.setState({
        sidebarWidth: currentSidebarWidth
      });
    }
  }

  render () {
    const {value} = this.props;
    const {sidebarWidth} = this.state;
    const keys = Object.keys(people).sort();
    const sliderSettings = {
      min: 0.5,
      max: 1.5,
      step: 0.05
    };
    const percentage = value - sliderSettings.min;
    const x = percentage * (sidebarWidth - ((DOT_WIDTH * 2) + SIDE_MARGIN));
    const preStyle = {
      transform: `translateX(${x - DOT_OFFSET}px)`
    };
    const postStyle = {
      transform: `translateX(${x + DOT_OFFSET}px)`
    };



    const orderedPeople = {};
    let next = people[keys[0]];
    keys.map(key => {
      const person = people[key];
      const name = person.name;

      if (name && !orderedPeople[name]) {
        next = name;
        orderedPeople[next] = {
          values: [],
        }
      }
      orderedPeople[next].values.push(key);
    });

    return (
      <div className='slider'>
        <div className='slider-container'>
          <input
            type='range'
            value={value}
            onChange={this.handleChangeValue}
            {...sliderSettings}
            title={'Select reading speed'}
          />
          <div className='slider-track slider-track-pre' style={preStyle}/>
          <div className='slider-track slider-track-post' style={postStyle}/>
        </div>
        <div className='slider--label'>
          <h1 className='slider-label--header'>Speed settings:</h1>
          <div className='people-container'>
            {Object.keys(orderedPeople)
              .map(name => {
                const person = orderedPeople[name];
                const isActive = person.values.indexOf(value.toString()) !== -1;
                const personClasses = cx(
                  'slider-label--person',
                  {
                    'slider-label--person__active': isActive
                  });
                return (
                  <button
                    onClick={() => {
                      this.props.showPersonBookmark(person.values[person.values.length - 1])
                    }}
                    className={personClasses}
                    key={name}
                    tabIndex={isActive ? 0 : -1}
                  >
                    <div className='slider-label-person--values'>
                      {person.values
                        .sort()
                        .map(currentValue => {
                          const classes = cx(
                            'slider-label-person--value',
                            {
                              'slider-label-person--value__active': currentValue === value.toString()
                            });
                          return (
                            <span key={currentValue} className={classes}>
                              {Math.round((currentValue - .5) * 100) / 10}
                            </span>
                          );
                        })}
                    </div>
                    <span className='slider-label-person--name'>{name}</span>
                  </button>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
};

module.exports = Slider;
