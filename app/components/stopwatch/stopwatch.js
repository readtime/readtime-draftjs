// SOURCE: https://gist.github.com/knjcode/9081842644bc0b49a180
import React from 'react';
import createReactClass from 'create-react-class';
import SidebarButton from '../sidebar/sidebar-button.js';
import Time from '../time/time.js';
import {getTimeValues} from '../../utils/calculate.js';
import { Circle } from 'rc-progress';
import ReactGA from 'react-ga';
import people from '../../constants/people.js';


const INTERVAL = (1000 / 60) * 4;

require('./stopwatch.less');

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isRunning: false,
      elapsed: 0,
      diff: 0
    };

    this.handleClickPausePlay = this.handleClickPausePlay.bind(this);
    this.handleClickReset = this.handleClickReset.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
    this.handleClickUseSpeed = this.handleClickUseSpeed.bind(this);
    this.tick = this.tick.bind(this);
  }

  componentWillUnmount () {
    clearInterval(this.state.timer);
  }

  tick () {
    this.setState({
      elapsed: Date.now() - this.state.start + this.state.diff
    });
  }

  handleClickPausePlay () {
    if (!this.state.isRunning) { // start
      const timer = setInterval(this.tick, INTERVAL);
      this.setState({
        isRunning: true,
        timer: timer,
        start: Date.now(),
      });

      ReactGA.event({
        category: 'timer',
        action: 'start',
      });
    } else { // pause
      clearInterval(this.state.timer);
      this.setState({
        timer: undefined,
        isRunning: false,
        diff: this.state.elapsed,
      });
      ReactGA.event({
        category: 'timer',
        action: 'pause',
      });
    }
  }

  handleClickReset () {
    this.reset()
  }

  reset() {
    clearInterval(this.state.timer);
    this.setState({
      timer: undefined,
      isRunning: false,
      elapsed: 0,
      diff: 0,
    });
    ReactGA.event({
      category: 'timer',
      action: 'reset',
    });
  }

  renderTimer() {
    const elapsed = this.state.elapsed / 1000;
    const timeProps = getTimeValues(elapsed, 2);
    const targetTime = this.props.time.totalSeconds;
    const percentage = elapsed / targetTime;

    const circleProps = {
      percent: percentage * 100,
      strokeWidth: 1,
      trailWidth: 1,
      prefixCls: 'stopwatch',
      trailColor: '#009fe3'
    };

    if (percentage >= 1) {
      circleProps.strokeColor = '#cf223c';
      circleProps.strokeWidth =  Math.min(percentage, 3);
    }

    return (
      <div className='stopwatch--time-wrap'>
        <Circle {...circleProps} />
        <Time
          showUnit={false}
          zeroPad={true}
          parentElementName='stopwatch'
          time={timeProps}/>
        <div className='stopwatch--target'>
          <p className='stopwatch-target--title'>This should take you:</p>
          <Time
            showUnit={false}
            parentElementName='stopwatch-target'
            time={this.props.time}/>
        </div>
      </div>
    );
  }

  handleClickUseSpeed () {
    this.props.onClickSaveSpeed(this.state.elapsed / 1000);
  }

  handleClickClose () {
    this.props.onClickClose();
  }

  renderButtons () {
    const {isRunning} = this.state;
    const commands = [
      {
        command: isRunning ? 'pause' : 'play',
        label: isRunning ? 'Pause' : 'Play',
        onClick: this.handleClickPausePlay
      },
      {
        command: 'reset',
        label: 'Reset',
        onClick: this.handleClickReset
      },
      {
        command: 'use',
        icon: 'tick',
        label: 'Use your speed',
        onClick: this.handleClickUseSpeed,
        disabled: (!this.state.elapsed)
      },
      {
        command: 'quit',
        label: 'Close',
        onClick: this.handleClickClose
      }
    ];

    return (
      <div className='sidebar--buttons' key='buttons-container'>
        {commands.map(button => {
          return <SidebarButton
            key={button.command}
            focusedButton={this.props.focusedButton}
            {...button}
          />
        })}
      </div>
    );
  }

  render () {
    return (
      <div className='stopwatch'>
        {this.renderTimer()}
        {this.renderButtons()}
      </div>
    );
  }
}


