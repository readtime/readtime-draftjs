import React from 'react';
import Immutable from 'immutable';
import Draft, {
  convertToRaw,
  convertFromRaw,
  Editor,
  EditorState,
  RichUtils,
  getDefaultKeyBinding,
  DefaultDraftBlockRenderMap,
  KeyBindingUtil,
  AtomicBlockUtils,
} from 'draft-js';
import {Helmet} from "react-helmet";
import ReactGA from 'react-ga';

import MobileMenu from '../../components/mobile-menu/mobile-menu.js';
import Bookmark from '../../components/bookmark/bookmark.js';
import Break from '../../components/break/break.js';
import Sidebar from '../../components/sidebar/sidebar.js';
import Person from '../../components/person/person.js';
import ShareView from '../../components/share/share.js';

const {Map} = Immutable;
const {hasCommandModifier} = KeyBindingUtil;

/* Utils */
import calculate from '../../utils/calculate.js';
import extractRawTextFromEditorState from '../../utils/extract-raw-text-from-editorstate.js';
import getTotalBreakTimes from '../../utils/get-total-break-times.js';
import convertOldToContentState from '../../utils/convert-old-data-to-content-state.js';
import findClosestPerson from '../../utils/find-closest-person.js';

/* Constants*/
import COMMANDS from '../../constants/commands.js';
import PEOPLE from '../../constants/people.js';

import CONTENT_HOME from '../../constants/content-home.js';
import CONTENT_ABOUT from '../../constants/content-about.js';
import CONTENT_LOADING from '../../constants/content-loading.js';
import {
  breakDescriptionPattern,
  descriptionPattern,
} from '../../constants/patterns.js';

const defaultSliderValue = 1;
const ppi = window.devicePixelRatio > 1 ? 2 : 1;
const variant = ppi === 2 ? '@2x' : '';

require('./editor-component.less');

const DEFAULT_BREAK = {
  TIME: 5,
  DESCRIPTION: '',
};

const hasLocalStorage = 'localStorage' in window;

const saveSliderValueToLocalStorage = (value) => {
  if (!hasLocalStorage) {
    return;
  }
  localStorage.setItem('readtime-slider-value', value);

};

const getSliderValueFromLocalStorage = (defaultResponse) => {
  if (!hasLocalStorage) {
    return defaultResponse;
  }
  let response = localStorage.getItem('readtime-slider-value') || defaultResponse;
  return parseFloat(response);
};

const getBlockRendererFn = (getEditorState, handleChangeBreak, handleMouseOverBreak, handleMouseOutBreak, getIsEditable) => (block) => {
  const type = block.getType();
  if (type === 'atomic') {
    const [
      time,
      description,
    ] = block
      .getText()
      .split(breakDescriptionPattern);
    return {
      component: Break,
      editable: false,
      props: {
        onFocus: handleMouseOverBreak,
        onBlur: handleMouseOutBreak,
        onChange: handleChangeBreak,
        getEditorState,
        time,
        description,
        disabled: getIsEditable(),
      },
    };

  }
  return null;
};

const blockRenderMap = Map({
  'header-one': {
    element: 'h1',
  },
  'break': {
    element: 'div',
  },
  'unstyled': {
    element: 'div',
  },
}).merge(DefaultDraftBlockRenderMap);

const getBlockStyle = block => {
  const type = block.getType();
  switch (type) {
    case 'atomic':
      return 'editor-part--break';
    case 'header-one':
      return 'editor-part editor-part--h1';
    case 'header-two':
      return 'editor-part editor-part--h2';
    default:
      return 'editor-part editor-part--text';
  }
};

const styleMap = {
  DESCRIPTION: {
    color: '#009fe3',
  },
};

const findFocusedButton = (editorState) => {
  let response = null;
  const currentSelection = editorState.getSelection();
  const startKey = currentSelection.getStartKey();
  const endKey = currentSelection.getEndKey();
  const isSameBlock = startKey === endKey;

  if (isSameBlock) {
    const currentStyle = editorState.getCurrentInlineStyle();
    const isDescription = currentStyle.has('DESCRIPTION');
    if (isDescription) {
      response = 'DESCRIPTION';
    }
  }
  return response;
};

const findTargetPage = (pathname) => {
  const routeSplit = pathname.split('/');
  const route = routeSplit.slice(1, routeSplit.length);
  const isRouted = route.length;
  let page = '';

  if (isRouted) {
    let page = route[0];
    if (page === 'x') {
      page = {
        main: 'saved-text',
        id: route[1],
      }
    }
    return page;
  }
  return page;
};

const fetchData = (id) => {
  const API_URL = 'http://readtime.eu/api/';

  const data = new FormData();
  data.append('action', 'get');
  data.append('id', id);

  return fetch(`${API_URL}`, {
    method: 'POST',
    body: data,
  })
    .then(response => response.json())
    .then(jsonResponse => {
      return jsonResponse;
    })
    .catch(error => {
      console.error(error);
    });
};

const setData = (text, sliderValue) => {
  const API_URL = 'http://readtime.eu/api/';

  const data = new FormData();
  data.append('action', 'set');
  data.append('text', JSON.stringify(text));
  data.append('slidervalue', sliderValue);

  return fetch(`${API_URL}`, {
    method: 'POST',
    body: data,
  })
    .then(response => response.json())
    .then(jsonResponse => {
      return jsonResponse;
    })
    .catch(error => {
      console.error(error);
    });
};

const getContent = (pathname) => {
  let content = CONTENT_HOME;
  const targetPage = findTargetPage(pathname);
  if (typeof targetPage === 'object') {
    if (targetPage.main === 'saved-text') {
      content = CONTENT_LOADING;
    }
  } else if (targetPage === 'about') {
    content = CONTENT_ABOUT;
  }
  const convertedContent = convertOldToContentState(content);
  return convertFromRaw(convertedContent);
};

let ogType;
let ogSiteName;
let ogUrl;
let ogImage;

export default class EditorComponent extends React.Component {
  constructor(props) {
    super(props);

    const {location: {pathname}} = props;
    const content = getContent(pathname);
    const targetPage = findTargetPage(pathname);
    const targetPageIsObject = typeof targetPage === 'object';

    let isLoading = false;
    if (targetPageIsObject) {
      if (targetPage.main === 'saved-text') {
        isLoading = true;
      }
    }

    this.state = {
      savedItemId: targetPage.id,
      showShareBookmark: false,
      showPersonBookmark: false,
      activeSidebarView: 'default',
      isHoveringBreak: false,
      sliderValue: defaultSliderValue,
      editorState: EditorState.createWithContent(content),
      isInitial: !targetPageIsObject || isLoading,
      sidebarOpened: false,
    };

    this.focus = this.focus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.setOgMeta = this.setOgMeta.bind(this);

    this.closeSidebar = this.closeSidebar.bind(this);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.handleMouseOverBreak = this.handleMouseOverBreak.bind(this);
    this.handleMouseOutBreak = this.handleMouseOutBreak.bind(this);
    this.handleChangeSlider = this.handleChangeSlider.bind(this);
    this.handleChangeBreak = this.handleChangeBreak.bind(this);
    this.handleClickAddBreak = this.handleClickAddBreak.bind(this);
    this.handleClickAddDescription = this.handleClickAddDescription.bind(this);
    this.handleClickClosePersonBookmark = this.handleClickClosePersonBookmark.bind(this);
    this.handleClickCloseShareBookmark = this.handleClickCloseShareBookmark.bind(this);
    this.handleClickShare = this.handleClickShare.bind(this);
    this.handleClickUseYourSpeed = this.handleClickUseYourSpeed.bind(this);
    this.handleClickEdit = this.handleClickEdit.bind(this);
    this.handleClickTwitter = this.handleClickTwitter.bind(this);
    this.handleClickFacebook = this.handleClickFacebook.bind(this);
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
    this.handleFacebookResponse = this.handleFacebookResponse.bind(this);
    this.showPersonBookmark = this.showPersonBookmark.bind(this);
    this.setSidebarView = this.setSidebarView.bind(this);

    this.blockRenderFn = getBlockRendererFn(this.getEditorState, this.handleChangeBreak, this.handleMouseOverBreak, this.handleMouseOutBreak, this.getIsInitial).bind(this);
    ReactGA.initialize('UA-29589349-1');
  }


  componentWillUnmount() {
    clearTimeout(this.saveSliderValueToLocalStorageTimeout);
    clearTimeout(this.showPersonBookmarkTimeout);
  };

  componentDidMount() {
    ogType = document.querySelector('meta[property="og:type"]');
    ogSiteName = document.querySelector('meta[property="og:site_name"]');
    ogUrl = document.querySelector('meta[property="og:url"]');
    ogImage = document.querySelector('meta[property="og:image"]');

    const sliderValue = getSliderValueFromLocalStorage(defaultSliderValue);
    this.setState({
      sliderValue,
    });
    this.focus();

    window.onfocus = this.focus;
    this.checkDeeplink(this.props.location.pathname);
  }

  focus = () => this.refs.editor.focus();
  getEditorState = () => this.state.editorState;
  getIsInitial = () => this.state.isInitial;

  checkDeeplink(path) {
    const targetPage = findTargetPage(path);
    if (targetPage.main === 'saved-text') {
      fetchData(targetPage.id)
        .then(response => {
          const convertedContent = convertOldToContentState(JSON.parse(response.text));
          const text = convertFromRaw(convertedContent);
          this.setState({
            sliderValue: parseFloat(response.slidervalue),
            editorState: EditorState.createWithContent(text),
            isInitial: false,
          }, this.setOgMeta)
        })
    }
  }

  handleChange(editorState, callback) {
    let focusedButton = findFocusedButton(editorState);
    this.setState({
      focusedButton,
      editorState,
    }, () => {
      (typeof callback === 'function') && callback();
    });
  }

  handleChangeSlider(newValue) {
    if (newValue === this.state.sliderValue) {
      return;
    }
    this.setState({
      sliderValue: newValue,
    });
    ReactGA.event({
      category: 'slider',
      action: 'change',
      label: newValue,
    });

    clearTimeout(this.saveSliderValueToLocalStorageTimeout);
    this.saveSliderValueToLocalStorageTimeout = setTimeout(saveSliderValueToLocalStorage.bind(null, newValue), 4000);
  }

  handleChangeBreak(block, newValue, getEditorState) {
    this.updateMetaData(block, newValue, getEditorState());
  }

  updateMetaData(block, metadata, editorState) {
    const blockKey = block.getKey();
    let contentState = editorState.getCurrentContent();
    const updatedBlock = contentState
      .getBlockForKey(blockKey)
      .mergeIn(['text'], metadata);

    let blockMap = contentState.getBlockMap();
    blockMap = blockMap.merge({[blockKey]: updatedBlock});
    contentState = contentState.merge({blockMap});

    const newEditorState = EditorState.push(editorState, contentState, 'metadata-update');
    this.handleChange(newEditorState);
  };

  updateUrl() {
    const {savedItemId} = this.state;
    if (savedItemId) {
      const location = savedItemId ? `/x/${savedItemId}` : '/';
      history.pushState({}, 'Home', location);
    }
  }

  setOgMeta() {
    ogUrl.content = `http://readtime.eu/x/${this.state.savedItemId}`;
  }

  handleMouseOverBreak() {
    this.setState({isHoveringBreak: true})
  }

  handleMouseOutBreak() {
    this.setState({isHoveringBreak: false})
  };

  handleKeyCommand(command) {

    if (command === 'description') {

      this.toggleDescription();
      return 'handled';
    } else if (command === 'break') {
      this.addBreak();
      return 'handled';
    } else if (command === 'stopwatch') {
      this.setSidebarView('stopwatch');
      return 'handled';
    }

    const { editorState } = this.state
    const newState = RichUtils.handleKeyCommand(editorState, command)

    if (newState) {
      this.handleChange(newState)
      return 'handled'
    }

    return 'not-handled';
  };

  keyBindingFn(event) {
    if (hasCommandModifier(event)) {
      const type = COMMANDS
        .find(obj => obj.keyCode === event.keyCode);
      return type && type.command || getDefaultKeyBinding(event);
    }
    return getDefaultKeyBinding(event);
  };

  handleClickClosePersonBookmark() {
    this.hidePersonBookmark();
  }

  handleClickCloseShareBookmark() {
    this.hideShareBookmark();
  }

  handleClickAddBreak() {
    this.addBreak();
    this.closeSidebar()
  }

  handleClickAddDescription() {
    this.toggleDescription();
  }

  handleClickUseYourSpeed(value) {
    this.handleChangeSlider(value);
    this.showPersonBookmark();

    ReactGA.event({
      category: 'sidebar',
      action: 'use speed',
    });
  }

  handleClickEdit() {
    this.setState({
      isInitial: false,
    });
    this.handleChange(EditorState.createEmpty(), this.focus);

    ReactGA.event({
      category: 'initial',
      action: 'edit',
    });
  }

  handleClickShare() {
    this.saveToDatabase(this.setOgMeta);
    this.showShareBookmark();
  }

  toggleInlineStyle(inlineStyle, callback) {
    this.handleChange(
      RichUtils.toggleInlineStyle(
        this.state.editorState,
        inlineStyle,
      ),
      callback,
    )
  }

  saveToDatabase(callback) {
    const rawData = convertToRaw(this.state.editorState.getCurrentContent());
    setData(rawData, this.state.sliderValue)
      .then(response => {
        if (response.error) {
          return this.handleSaveError(response.error);
        }
        return this.handleSaveSuccess(response.data, callback);
      });
  };

  handleSaveSuccess(data, callback) {
    this.setState({
      savedItemId: data.id,
    });
    this.updateUrl();
    callback && callback()
  };

  handleSaveError(error) {
    throw new Error(error)
  };

  setSidebarView(targetView) {
    this.setState({
      activeSidebarView: targetView || (this.state.activeSidebarView === 'default' ? 'stopwatch' : 'default'),
    });
  };

  addBreak() {
    const {TIME, DESCRIPTION} = DEFAULT_BREAK;
    const {editorState} = this.state;
    const selectionState = editorState.getSelection();

    const contentState = editorState.getCurrentContent();
    const startKey = selectionState.getStartKey();
    const endKey = selectionState.getEndKey();

    let lastWasEnd = false;
    const selections = contentState
      .getBlockMap()
      .skipUntil(block => block.getKey() === startKey)
      .takeUntil(block => {
        const result = lastWasEnd;
        if (block.getKey() === endKey) {
          lastWasEnd = true;
        }
        return result;
      })
      .map(block => {
        const key = block.getKey();
        let text = block.getText();
        let start = 0;
        let end = text.length;

        if (key === startKey) {
          start = selectionState.getStartOffset();
        }
        if (key === endKey) {
          end = selectionState.getEndOffset();
        }
        return text.slice(start, end);
      }).toJS();
    const selectedText = Object.values(selections).join(' ');

    const text = `${TIME}${breakDescriptionPattern}${selectedText || DESCRIPTION}`;
    const contentStateWithEntity = contentState.createEntity(
      'atomic',
      'IMMUTABLE',
      {});
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newState = AtomicBlockUtils.insertAtomicBlock(editorState, entityKey, text);
    this.handleChange(newState);

    ReactGA.event({
      category: 'click',
      action: 'break',
    });
  }

  toggleDescription() {
    this.toggleInlineStyle('DESCRIPTION', this.focus);

    ReactGA.event({
      category: 'click',
      action: 'toggle description',
    });
  };

  calculateTime() {
    const currentContent = this.state.editorState.getCurrentContent();
    const rawContent = convertToRaw(currentContent);
    const text = extractRawTextFromEditorState(rawContent);
    const breakTimes = getTotalBreakTimes(rawContent);

    return calculate({
      text,
      breakTimes,
      multiplier: this.state.sliderValue - 0.25,
    });
  }

  toggleSidebar() {
    ReactGA.event({
      category: 'sidebar',
      action: 'toggle',
      label: this.state.sidebarOpened ? 'close' : 'open',
    });
    this.setState({
      sidebarOpened: !this.state.sidebarOpened,
    });
  }

  closeSidebar() {
    ReactGA.event({
      category: 'sidebar',
      action: 'toggle',
      label: 'close',
    });
    this.setState({
      sidebarOpened: false,
    });
  }

  showPersonBookmark() {
    this.setState({
      showPersonBookmark: true,
      showShareBookmark: false,
    });
    ReactGA.event({
      category: 'bookmark',
      action: 'person',
      label: 'show',
    });
  }

  hidePersonBookmark() {
    this.setState({
      showPersonBookmark: false,
    });
    ReactGA.event({
      category: 'bookmark',
      action: 'person',
      label: 'hide',
    });
  }

  showShareBookmark() {
    this.setState({
      showShareBookmark: true,
      showPersonBookmark: false,
    });

    ReactGA.event({
      category: 'bookmark',
      action: 'share',
      label: 'show',
    });
  }

  hideShareBookmark() {
    this.setState({
      showShareBookmark: false,
    });

    ReactGA.event({
      category: 'bookmark',
      action: 'share',
      label: 'hide',
    });
  };

  renderPersonBookmark() {
    if (!this.state.showPersonBookmark) {
      return null;
    }
    const selectedPerson = {
      variant,
      ...findClosestPerson({
        people: PEOPLE,
        personKey: this.state.sliderValue,
      }),
    };
    return (
      <Bookmark
        showCut={true}
        onClickClose={this.handleClickClosePersonBookmark}
      >
        <Person {...selectedPerson} />
      </Bookmark>
    );
  }

  renderShareBookmark() {
    const {showShareBookmark, savedItemId, editorState} = this.state;
    if (!showShareBookmark) {
      return null;
    }
    return (
      <Bookmark
        showCut={false}
        onClickClose={this.handleClickCloseShareBookmark}
      >
        <ShareView
          savedItemId={savedItemId}
          handleClickTwitter={this.handleClickTwitter}
          handleClickFacebook={this.handleClickFacebook}
          text={convertToRaw(editorState.getCurrentContent())}
        />
      </Bookmark>
    );
  }

  handleClickTwitter() {
    const link = document.createElement('a');
    link.href = `https://twitter.com/intent/tweet?text=${"I have written a text using http://readtime.eu. Check it out!"}&url=http://readti.me/x/${this.state.savedItemId}&via=readtime_eu`;
    link.target = '_blank';
    document.body.appendChild(link);
    link.click();


    ReactGA.event({
      category: 'social',
      action: 'share',
      label: 'twitter',
    });
  }

  handleClickFacebook() {
    FB.ui({
      method: 'share',
      href: `http://readti.me/x/${this.state.savedItemId}`,
      hash: '#readtime',
    }, response => this.handleFacebookResponse);
  }

  handleFacebookResponse() {
    ReactGA.event({
      category: 'social',
      action: 'share',
      label: 'facebook',
    });
  }

  render() {
    const time = this.calculateTime();
    const {
      isHoveringBreak,
      activeSidebarView,
      focusedButton,
      editorState,
      sliderValue,
      savedItemId,
      isInitial,
      sidebarOpened,
    } = this.state;

    const editorProps = {
      blockStyleFn: getBlockStyle,
      customStyleMap: styleMap,
      editorState,
      handleKeyCommand: this.handleKeyCommand,
      onChange: this.handleChange,
      placeholder: 'Tell me a story',
      ref: 'editor',
      spellCheck: true,
      keyBindingFn: this.keyBindingFn,
      blockRendererFn: this.blockRenderFn,
      blockRenderMap,
      readOnly: isInitial || isHoveringBreak,

    };

    return (
      <div className='editor-component'>
        <Sidebar
          setView={this.setSidebarView}
          activeView={activeSidebarView}
          focusedButton={focusedButton}
          time={time}
          showPersonBookmark={this.showPersonBookmark}
          editorState={editorState}
          sliderValue={sliderValue}
          onChangeSlider={this.handleChangeSlider}
          onClickAddBreak={this.handleClickAddBreak}
          onClickAddDescription={this.handleClickAddDescription}
          onClickShare={this.handleClickShare}
          onClickUseYourSpeed={this.handleClickUseYourSpeed}
          savedItemId={savedItemId}
          isInitial={isInitial}
          open={sidebarOpened}
          close={this.closeSidebar}
          toggle={this.toggleSidebar}
        />

        <div className='editor'>
          <div className='editor-wrapper'>
            <div className='editor-content' onClick={this.focus}>
              {React.Children.toArray(this.props.children)}
              <Editor {...editorProps} />
              {isInitial &&
              <button className='button button-enable-edit' onClick={this.handleClickEdit}>
                Start timing
              </button>}
              {this.renderShareBookmark()}
              {this.renderPersonBookmark()}
            </div>
          </div>
          <MobileMenu
            time={time}
            onClickAddBreak={this.handleClickAddBreak}
            onClickAddDescription={this.handleClickAddDescription}
            onClickShare={this.handleClickShare}
            isInitial={isInitial}
            focusedButton={focusedButton}
            onClickToggleSidebar={this.toggleSidebar}
          />
        </div>
      </div>
    )
  }
};
