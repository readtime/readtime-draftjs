/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';


/* Styles */
require('../../less/style.less');


export default class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { id } = this.props.routeParams;
    return (
      <div className="home"></div>
    );

  }
}
