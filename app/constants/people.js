export default {
  '0.5': {
    name: 'Busta Rhymes',
    bio: 'The king of speed rapping. In 2000, Busta Rhymes was entered into the Guinness Book of World Records for having said the most syllables in one second.',
    img: 'busta-rhymes'
  },
  '0.55': {},
  '0.6': {},
  '0.65': {},
  '0.7': {
    name: 'Eminem',
    bio: 'Your pace matches that of the 00’s word wizard champion. Eminem\'s poetic talents as well as his emotional and artistic range are unrivalled.',
    img: 'eminem'
  },
  '0.75': {},
  '0.8': {},
  '0.85': {},
  '0.9': {
    name: 'Ellen Degeneres',
    bio: 'With her own talk show, Ellen DeGeneres is one of America\'s most well-known comedians, also serving as a prominent voice in the gay/lesbian comunity.',
    img: 'ellen-degeneres'
  },
  '0.95': {},
  '1': {
    name: 'Average Joe',
    bio: 'This phase is known as the average reading speed. Calculated and defined with the help of multiple users. It’s a safe setting when working with multiple actors.'
  },
  '1.05': {},
  '1.1': {},
  '1.15': {
    name: 'Steve Jobs',
    bio: 'Your pace matches that of legendary speaker Steve Jobs. Famed for many groundbreaking keynotes that defined pivotal moments in history.',
    img: 'steve-jobs'
  },
  '1.2': {},
  '1.25': {},
  '1.3': {
    name: 'Barack Obama',
    bio: 'The sweet spot of public speaking. Being one of the greatest orators of our time, to describe his Acceptance Speech as “electrifying” would hardly be an overstatement.',
    img: 'barack-obama'
  },
  '1.35': {},
  '1.4': {},
  '1.45': {},
  '1.5': {
    name: 'Sir David Attenborough',
    bio: 'Slow is sweet. King of narrative speech, he is able to paint pictures through his words.',
    img: 'sir-david-attenborough'
  }
};
