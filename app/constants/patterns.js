/**
 * Created by MettinParzinski on 29/11/2016.
 */
"use strict";

module.exports = {
  breakPattern: /\[BREAK=(\d)+\]/gi,
  breakPatternNoCapture: /\[BREAK=[[\d]*]*]/gi,

  descriptionPattern: /\{\{\{([\s\S]*?)}}}/gi,
  descriptionPatternNoCapture: /\{\{\{(?:[\s\S])*?}}}/gi,

  spanDescriptionPattern: /<span class="(?:.)*?">(.+?)<\/span>/g,
  spanPattern: /<(\/)*span>/g,

  breakDescriptionPattern: '//:://',
};
