export default {
  blocks: [
    {
      text: `Loading your data`,
      type: 'header-one',
    },
    {
      text: 'This shouldn\'t take that long.',
      type: 'unstyled'
    }
  ],
  entityMap: {}
}

