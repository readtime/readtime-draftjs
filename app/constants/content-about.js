export default {
  blocks: [
    {
      text: `About Readtime`,
      type: 'header-one',
    },
    {
      text: 'Readtime is the only tool to help you calculate how long it will take you to read a text out loud.',
      type: 'unstyled'
    }
  ],
  entityMap: {}
}

