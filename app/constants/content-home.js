export default {
  blocks: [
    {
      text: `This intro will take 52.03 seconds to read out loud`,
      type: 'header-one',
    },
    {
      text: 'Welcome to Readtime, the tool to help calculate how long it will take to read text out loud. Perfectly set up to provide insight and control when working on presentations, film or radio scripts.',
      type: 'unstyled'
    },
    {
      text: 'Quick Tips & tricks',
      type: 'header-one'
    },
    {
      text: 'Get the most out of Readtime. Select a famous speaker that matches your speed, or the speed of your actor. You can also clock your reading time to calculate your personal reading pace.',
      type: 'unstyled'
    },
    {
      text: '8//:://Add breaks and describe what happens in this section.',
      type: 'atomic'
    },
    {
      text: 'You can also mark text as a description. The blue text won\'t be added to the calculation of your total reading time. Perfect for describing the scene.',
      type: 'unstyled',
      inlineStyleRanges: [
        {
          length: 40,
          offset: 0,
          style: 'DESCRIPTION'
        }
      ]
    },
    {
      text: 'If you\'re writing a speech, planning a presentation or writing a script and are thinking: how long does it take to read this text (out loud); Readtime is the best tool for you!',
      type: 'unstyled'
    },
    {
      text: 'Enjoy your newly renovated Readtime and let us know what you think of it.',
      type: 'unstyled'
    },
  ],
  entityMap: {}
}

