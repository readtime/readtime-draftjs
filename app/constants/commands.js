const KEYCODES = [
  {
    command: 'break',
    label: 'Break',
    keyCode: 13,
    button: 'enter'
  },
  {
    command: 'description',
    label: 'Description',
    keyCode: 191,
    button: '/'
  },
  {
    command: 'stopwatch',
    label: 'Stopwatch',
    keyCode: 66,
    button: 'b'
  },
  {
    command: 'share',
    label: 'Save & Share',
    keyCode: 101,
    button: 's'
  }
];

export default KEYCODES;