export default (rawState) => {

  let combinedText = [];
  rawState.blocks
    .filter(block => block.type === 'unstyled')
    .map(block => {
      let text = block.text;
      const descriptionOffsets = block.inlineStyleRanges
        .filter(styleRange => styleRange.style === 'DESCRIPTION');
      descriptionOffsets.push({
        offset: text.length
      });

      if (descriptionOffsets.length) {
        let totalOffset = 0;
        const textWithoutDescriptions = descriptionOffsets.map((description) => {
          const response = text.slice(totalOffset, description.offset);
          totalOffset = description.offset + description.length;
          return response;
        });
        textWithoutDescriptions && combinedText.push(textWithoutDescriptions.join(''));
      } else {
        text && combinedText.push(text);
      }
    });
  return combinedText.join('');
};