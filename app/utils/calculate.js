'use strict';
const wordLengthTimes = [
  0,
  0.1,
  0.2,
  0.3,
  0.4,
  0.5,
  0.6,
  0.7,
  0.8,
  0.9,
  1.0,
  1.05,
  1.10,
  1.15,
  1.20,
  1.25,
  1.30,
  1.35,
  1.40,
  1.45,
  1.50,
  1.55,
  1.60,
  1.65,
  1.70,
  1.75,
  1.80
];
const timePerSpecialCharacter = {
  point: .05,
  comma: .02
};

const specialCharactersPattern = /[!@#$%^&*()_+-=\{\};:\'\"\\\|<>/\?.,]/g;
const descriptionPattern = /\{\{\{([^\d]*)\}\}\}/gi;

const removeDescriptions  = (text) => {
  return text.replace(descriptionPattern, '');
}

const extractSpecialCharacters  = (text) => {
  return text.replace(specialCharactersPattern, '');
}

const countCharacter  = (text, character) => {
  return text.split(character).length - 1;
}

const getWordOccurrences  = (text) => {
  let words = text.split(/ /g);
  let amountOfWordsPerLength = {};

  words.map((word) => {
    if (word) { // decline spaces
      let length = word.length;
      return amountOfWordsPerLength[length] = (amountOfWordsPerLength[length] || 0) + 1
    }
  });

  return amountOfWordsPerLength;
}

const roundUp = (num) => Math.round((num) * 100) / 100


const calculateWordTimes = (wordOccurances) => {
  let result = 0;

  Object.keys(wordOccurances).map((words) => {
    const duration = wordLengthTimes[words] || wordLengthTimes.slice(-1)[0];
    const amount = wordOccurances[words];
    const number = roundUp(amount * duration)

    result += number
  });
  return roundUp(result);
}

const calculateSpecialCharactersTime = (specialCharacterCount, timePerSpecialCharacter) => {
  let result = 0;

  Object.keys(specialCharacterCount).map((specialCharacter) => {
    const timeForThisCharacter = timePerSpecialCharacter[specialCharacter];
    const occurrances = specialCharacterCount[specialCharacter];
    result += timeForThisCharacter * occurrances;
  });

  return roundUp(result);
}

const getTimeValues = (totalSeconds, maxMillisecondsLength = 1) => {
  const roundedSeconds = Math.floor(totalSeconds);

  const days = Math.floor((totalSeconds / 3600) / 24);
  const hours = Math.floor(totalSeconds / 3600);
  const minutes = Math.floor((totalSeconds % 3600) / 60);
  const seconds = Math.floor((totalSeconds % 3600) % 60);

  const milliseconds = Math.round(String(totalSeconds - roundedSeconds).substr(2, maxMillisecondsLength));
  return {
    hours,
    minutes,
    seconds,
    milliseconds,
    days,
    totalSeconds
  };
}

const calculate = ({text, multiplier = 1, breakTimes}) => {
  if (!text) {
    return getTimeValues(0);
  }

  let specialCharacterCount = {
    comma: countCharacter(text, ','),
    point: countCharacter(text, '.')
  };

  let textWithoutSpecialCharacters = extractSpecialCharacters(text);
  let textWithoutDescriptions = removeDescriptions(textWithoutSpecialCharacters);
  let wordOccurrences = getWordOccurrences(textWithoutDescriptions);

  let wordTimes = calculateWordTimes(wordOccurrences);
  let specialCharactersTime = calculateSpecialCharactersTime(specialCharacterCount, timePerSpecialCharacter);

  let totalSeconds = roundUp(((specialCharactersTime + wordTimes) * multiplier) + breakTimes);


  return getTimeValues(totalSeconds);
}

module.exports = calculate;
module.exports.getTimeValues = getTimeValues;
