import {
  breakPatternNoCapture,
  breakPattern,
  breakDescriptionPattern
} from '../constants/patterns.js';

export default (text = '') => {
  if (text && text.blocks && text.entityMap) {
    return text;
  }
  const breaks = text.match(breakPattern);
  const textWithoutBreaks = text.split(breakPatternNoCapture);
  const blocks = [];
  textWithoutBreaks.map((text, i) => {
    const brokenBlocks = text
      .split('\n')
      .filter(textPart => textPart !== '');

    brokenBlocks
     .map(textPart => ({
       text: textPart,
       type: 'unstyled'
     }));
    blocks.push(...brokenBlocks);

    const breakString = breaks && breaks[i];
    if (breakString) {
      const breakTime = Number(breakString
        .replace(/\[break\=/gi, '')
        .replace(/\]/, ''));

      blocks.push({
        type: 'atomic',
        text: `${breakTime}${breakDescriptionPattern}`
      })
    }
  });

  return {
    blocks,
    entityMap: []
  };
};