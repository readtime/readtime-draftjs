import expect from 'expect';
import extract from '../extract-raw-text-from-editorstate.js';

const fakeContent = {
  blocks: [
    {
      text: `123456`,
      type: 'unstyled',
      inlineStyleRanges: [
        {
          length: 3,
          offset: 3,
          style: 'DESCRIPTION'
        }
      ]
    }
  ],
  entityMap: {}
};

describe('extractRawTextFromEditorstate', () => {
  it('should return a raw text', () => {
    expect(extract(fakeContent)).toEqual(123)
  });
});