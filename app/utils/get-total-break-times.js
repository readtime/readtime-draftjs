import PATTERNS from '../constants/patterns.js';

export default (rawState) => {
  let breakTimes = 0;

  rawState.blocks
    .filter(block => block.type === 'atomic')
    .map(block => {
      const text = block.text;
      const split = text.split(PATTERNS.breakDescriptionPattern);
      breakTimes += parseInt(split[0] || 0, 10);
    });
  return breakTimes;
};