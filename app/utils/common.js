"use strict";

const selectTimeUnitsToShow = function(time = 0) {
  let response = {};
  let forced = [
    'seconds',
    'milliseconds'
  ];

  Object.keys(time).map((unitName) => {
    let unit = time[unitName];
    if (unitName === 'totalSeconds' || (!unit && (forced.indexOf(unitName) === -1))) {
      return null;
    }
    response[unitName] = unit.toString();
  });

  return (response);
};


const zeroPad = function(number, desiredLength = 2) {
  let numberZeroPadded = number.toString();
  while (numberZeroPadded.length < desiredLength) {
    numberZeroPadded = "0" + numberZeroPadded;
  }
  return numberZeroPadded;
};

export {
  selectTimeUnitsToShow,
  zeroPad
};


