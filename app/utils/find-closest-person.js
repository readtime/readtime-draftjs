const findClosestPerson = function ({people, personKey}) {
  const peopleKeys = Object.keys(people).sort();
  const orderedPeople = {};
  let nextPerson = people[peopleKeys[0]];

  peopleKeys.map(key => {
    const person = people[key];
    const name = person.name;

    if (name && !orderedPeople[name]) {
      nextPerson = name;
      orderedPeople[nextPerson] = {
        values: [],
      }
    }
    orderedPeople[nextPerson].values.push(key);
  });

  let response = null;
  Object.keys(orderedPeople)
    .forEach(name => {
      if (orderedPeople[name].values.indexOf(personKey.toString()) !== -1) {
        const personIndex = peopleKeys.find(person => people[person].name === name)
        response = people[personIndex]
      }
    });
  return response;
};


export default findClosestPerson;